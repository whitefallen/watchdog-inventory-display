import React, {useEffect, useState} from 'react';
import './ChestPool.css';
import Chest from "./Chest";

export default function ChestPool () {

  const [chests, setChests] = useState([])
  useEffect(() => {
    fetch('http://localhost:8000/api/getAllBlocks', {
      method: 'GET',
      mode: 'cors',
      cache: 'no-cache',
      headers: {
        'Content-Type' : 'application/json'
      }
    }).then(
      response => response.json()
    ).then(
      (data) => {
        console.log(data);
        setChests(data)
      }
    )
  }, [])

  return(
    <div className="chestpool">
      {chests.map((chest, index) => {
        return <Chest chestObj={chest} key={index}/>
      })}
    </div>
  )
}
