import React from "react";

export default function Location ({locationObj}) {
  return (
    <div>
      {locationObj.world} | {locationObj.x} | {locationObj.y} | {locationObj.z}
    </div>
  )
}
