import React from 'react';
import Location from "./Location";
import Inventory from "./Inventory";
import './Chest.css';
import chestImg from './Chest.png'
import Moment from 'moment';

export default function Chest ({chestObj}) {
  return (
    <div className="chestbox">
      <img src={chestImg} alt={"chestbox image"} />
      <div className="chestbox-content">
        <Location locationObj={chestObj.location}/>
        <Inventory inventoryArr={chestObj.inventory}/>
        <div className="chestbox-content-created">Last updated at {Moment(chestObj.updated_at).format('DD.MM.YYYY HH:mm')}</div>
      </div>
    </div>
  )
}
