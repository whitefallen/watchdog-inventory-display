import React from "react";
import './inventory.css'
export default function Inventory ({inventoryArr}) {
  return (
    <div className="inventory">
      {inventoryArr.map((inventory, index ) => {
        return <div key={index}>{inventory.type} x {inventory.amount}</div>
      })}
    </div>
  )
}
